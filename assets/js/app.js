// Add 'sticky class on header'
if ( $(window).width() > 992 ) {
    $(window).scroll(function(){
        if ($(this).scrollTop() > 0) {
            $('header').addClass('hide-header');
        } else {
            $('header').removeClass('hide-header');
        }
    });
}

// Menu bar
$('.bar-icon').click(function(){
    $('.responsive-nav').toggleClass('show-nav');
});


// glide js - logo slider
var glideMulti = new Glide('.multi', {
    type: 'carousel',
    autoplay: 3000,
    perView: 7
});
glideMulti.mount();

// slick slider - industries
$('.slider-nav').slick({
    slidesToShow: 7,
    slidesToScroll: 2,
    // asNavFor: '.slider-for',
    dots: true,
    autoplay: true,
    arrows: true,
    focusOnSelect: true,
    responsive: [
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 6
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 5
            }
        },
        {
            breakpoint: 575,
            settings: {
              slidesToShow: 3
            }
        },
        {
            breakpoint: 480,
            settings: {
              slidesToShow: 2
            }
        },
        {
            breakpoint: 360,
            settings: {
                slidesToShow: 1
            }
        }
    ]
});


// slick - boxslider
$('.slider-box').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    dots: true,
    autoplay: true,
    duration: 3000,
    responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 2
          }
        },
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 1
          }
        }
    ]
    // arrows: true,
    // focusOnSelect: true
});